import { Injectable } from '@nestjs/common';
import { request } from 'undici';

@Injectable()
export class AppService {
  getHello(): any {
    return { message: 'Hello World!' };
  }

  async getCat(): Promise<any> {
    const { body } = await request('https://cataas.com/cat?json=true');
    return body.json();
  }

  async getTags(): Promise<any> {
    const { body } = await request('https://cataas.com/api/tags');
    return body.json();
  }

  async getCutes(): Promise<any> {
    const { body } = await request('https://cataas.com/api/cats?tags=cute');
    return body.json();
  }
}
