import { Controller, Get } from '@nestjs/common';
import { randomUUID as uuuid, pbkdf2Sync, pbkdf2, randomBytes } from 'crypto';
import { AppService } from './app.service';
import * as util from 'util';

const promisedPbkdf2 = util.promisify(pbkdf2);

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Get('/uuid')
  public getUuid(): string {
    return uuuid();
  }

  @Get('/crypto')
  public crypto(): string {
    const salt = randomBytes(128).toString('base64');
    return pbkdf2Sync('mot_de_passe', salt, 10000, 512, 'sha512').toString(
      'base64',
    );
  }

  @Get('/cryptoAsync')
  public async cryptoAsync(): Promise<string> {
    const salt = randomBytes(128).toString('base64');
    const buff = await promisedPbkdf2(
      'mot_de_passe',
      salt,
      10000,
      512,
      'sha512',
    );
    return buff.toString('base64');
  }

  @Get('/login')
  public async login(): Promise<any> {
    const tags = await this.appService.getTags();

    const [cutes, cat] = await Promise.all([
      this.appService.getCutes(),
      this.appService.getCat(),
    ]);

    return {
      tags,
      cutes,
      cat,
    };
  }
}
